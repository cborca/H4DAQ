#ifndef NO_BCP
#ifndef BCP_H
#define BCP_H

// C++ middleware that glues together our Python <-> BCP Bridge
// with the rest of H4DAQ 

//#include "interface/StandardIncludes.hpp"
#include "interface/Board.hpp"
#include "interface/BoardConfig.hpp"

#include <string>

// Necessary Boost Libraries
#include <boost/python.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/algorithm.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <fstream>
#include <tuple> 
#include <map>
#include <array> 

// Rogue libraries
#include <rogue/interfaces/stream/Frame.h>
#include <rogue/interfaces/stream/FrameIterator.h>

namespace ris = rogue::interfaces::stream; 


using std::string; 

namespace bp = boost::python; 


struct subframe {
  std::array<uint64_t,15> channelData;
  uint64_t trailer;
};


//public TriggerBoard, IOControlBoard
//class BCP: public Board
class BCP : public TriggerBoard, IOControlBoard
{


  struct dfHeader {
    uint32_t clkhigh;
    uint32_t clklow; 
    uint32_t tsLSB;
    uint32_t tsMSB;
    uint32_t separator[3];
  };



public:

  BCP(bool pyFinalize=true, const std::string &ip="192.168.41.192", 
      unsigned serverPort=10000, bool debug = false,
      std::string path="/home/tanderso/bcp_controller_h4_oct2021",
      std::string roguepath="/home/tanderso/miniconda3/envs/H4DAQ/lib/python3.7/site-packages"); 

  ~BCP(); 
  virtual int Init();
  virtual int Clear();
  int ClearBusy();
  virtual int BufferClear(); //reset the buffers
  virtual int Print();
  virtual int Config(BoardConfig *bC);
  virtual int Read(vector<WORD> &v);
  virtual void SetHandle(int handle) { handle_= handle;};

  //Main functions to handle the event trigger
  virtual int SetBusyOn();
  virtual int SetBusyOff();
  virtual bool TriggerReceived();
  virtual int TriggerAck();
  virtual inline bool  SignalReceived(CMD_t signal) { return true; };
  virtual int SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus);
  virtual int SendHwCommand(CMD_t command, bool status) {return 0;};

  void setupReadout();
  void Readout(); 
  void ProcessFrames(); 
  void enableSWTrig(); 
  void disableSWTrig(); 
  bool readoutBusy();
  void readoutStatus(); 
  
protected:
  void setupPython();
  void cleanupPython();

  std::vector<uint32_t> packFrame(std::map<unsigned long, std::vector< subframe > > &store,
				  std::set<unsigned long> &lhcclocks); 

  
  std::string genPythonImports();
  std::map<std::string, bp::object> _pyfnMap;
  
  int StartDAQ();
  int StopDAQ();

private:

  uint32_t handle_;
  uint32_t fpgaversion_; 
  bool pyFinalize_;
  bool debug_; 
  bool sw_trigger_; 

  bp::object bcp_; 
  bp::object lpgbtfns_; 

  std::map<std::string, bp::object> bcpobjs_;
  std::vector<uint32_t> packedFrames_;
  std::string pypath_;
  std::string roguepath_; 
};


#endif
#endif

