#ifndef NO_FITPIX
#ifndef FITPIX_H
#define FITPIX_H

#include "interface/StandardIncludes.hpp"
#include "interface/Board.hpp"
#include "interface/BoardConfig.hpp"

#include "pxcapi.h"

#include <stdio.h>

#define FITPIX_SINGLE_CHIP_PIXSIZE 65536
#define FITPIX_ERRMSG_BUFF_SIZE    512

class FITPIX : public TriggerBoard, IOControlBoard
{
public:

    FITPIX() : TriggerBoard(), IOControlBoard() 
        {
            type_     = "FITPIX";
            _busyHandler_comPort_ = "";
        }

    virtual ~FITPIX()
	{
            if (_busyHandler_com)
                fclose(_busyHandler_com);
        }

    virtual int Init();
    virtual int Clear(); // stop + start
    virtual int BufferClear(); // stop + start
    virtual int ClearBusy();
    virtual int Config(BoardConfig *bc);
    virtual int Read(std::vector<WORD> &v);
    virtual void SetHandle(int) { return 0; }

    //Main functions to handle the event trigger
    virtual int SetBusyOn();
    virtual int SetBusyOff();
    virtual bool TriggerReceived();
    virtual int TriggerAck();
    virtual inline bool  SignalReceived(CMD_t signal) { return true; };
    virtual int SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus);
    virtual int SendHwCommand(CMD_t command, bool status) {return 0;};

protected:
    int Print();

    int SetEventHeader(std::vector<WORD> &v);

    void sparseReadout();
    void configDevice();
    /* reads params from cfg file */  
    int ParseConfiguration(BoardConfig * bc);

    int _n_devices;
    int _device_index;
    int _trigger_type; // 0: SW;  1: HW
    double _threshold;  // threshold in ADC counts
    int _sparse_readout;
    float _frame_duration;
    int _debug;
    double _clock_freq;
    int _tpx_mode; // 0: TOT, 1: TOA

    std::vector<WORD>_header;

    std::string _chip_configFile;
    std::string _busyHandler_comPort_;
  
    unsigned short _frame_buff[FITPIX_SINGLE_CHIP_PIXSIZE];
    unsigned int _frame_size;
    //sparse readout output
    unsigned int _mem_array[2*(FITPIX_SINGLE_CHIP_PIXSIZE+1)];
    unsigned short _n_pixels;
    unsigned int _mem_size;

    struct timespec bef_trig_time_;
    struct timespec aft_trig_time_;

    FILE* _busyHandler_com;        
};

#endif
#endif
