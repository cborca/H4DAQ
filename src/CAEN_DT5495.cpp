#include "interface/CAEN_DT5495.hpp"
#include <iostream>
#include <sstream>
#include <string>

//#define CAEN_DT5495_DEBUG
 
int CAEN_DT5495::Init()
{
    int status=0;
    ostringstream s; s << "[CAEN_DT5495]::[INFO]::++++++ CAEN DT5495 INIT ++++++";
    Log(s.str(),1);
  
    if (handle_<0)
        return ERR_CONF_NOT_FOUND;
    if (!IsConfigured())
        return ERR_CONF_NOT_FOUND;

    s.str(""); s <<"[CAEN_DT5495]::[INFO]::TRIG UNIT @ " << configuration_.IPAddress << " CONFIGURATION +++++" ;
    Log(s.str(),1);
  
    //Initializing Gate and Delay
    string DT_ip = configuration_.IPAddress;
    char *ip = &DT_ip[0u];
    status |= CAEN_PLU_OpenDevice(CAEN_PLU_CONNECT_DIRECT_ETH, ip, 0, 0, &handle_);

    //Reset All Registers
    for (int i = 0; i < sizeof(REGISTERS)/sizeof(REGISTERS[0]); i++) { 
        status |= WriteReg(REGISTERS[i], 0);
    }
 
    sleep(1);
  
    if (status) {
        s.str(""); s <<"[CAEN_DT5495]::[ERROR]::Communication error for device @ " << configuration_.IPAddress << " " << status ;
        Log(s.str(),1);
        return ERR_OPEN;
    }

    // enablePorts works in the opposite way: 
    //  - if we want an input disabled that port has to be set to 1 
    //  - if we want an input enabled, the correspond port has to be set to 0
    // It happens because a disabled port doesn't have to contribute to trigger decision.
    // That means sending always a signal or being fixed at 1.
    // We start with all the ports disabled: 0xFF = 255
    uint32_t enabledPorts = 255;
    uint32_t enabledBusy = 0;
  
    //Initializing Gate and Delay
    //string DT_ip = configuration_.IPAddress;
    //char *ip = &DT_ip[0u];
    //status |= CAEN_PLU_OpenDevice(CAEN_PLU_CONNECT_DIRECT_ETH, ip, 0, 0, &handle_);
    status |= CAEN_PLU_InitGateAndDelayGenerators(handle_);

    //Set gate and delay for the gdg based on types of the input

    s.str(""); s <<"Channel size: " << configuration_.Chl.size() ;
    Log(s.str(),1);

    for (int i = 0; i < configuration_.Chl.size(); i++){
        gdConverter (configuration_.Chl, i);
        if (configuration_.Chl[i].type == "Busy")
        {
            enabledBusy += pow(2., (configuration_.Chl[i].port));
            status |= CAEN_PLU_SetGateAndDelayGenerator(handle_, configuration_.Chl[i].port + 8, 1, configuration_.Chl[i].convertedGate, configuration_.Chl[i].convertedDelay, 1);
        }
        else if (configuration_.Chl[i].type == "TriggerInput")
        {
            enabledPorts -= pow(2., (configuration_.Chl[i].port));
            status |= CAEN_PLU_SetGateAndDelayGenerator(handle_, configuration_.Chl[i].port, 1, configuration_.Chl[i].convertedGate, configuration_.Chl[i].convertedDelay, 1);
            s.str(""); s <<"Converted Gate" << configuration_.Chl[i].convertedGate ;
            Log(s.str(),1); 
            s.str(""); s <<"Converted Delay" << configuration_.Chl[i].convertedDelay ;
            Log(s.str(),1); 
        }
        else if (configuration_.Chl[i].type == "TriggerOutput")
        {
            status |= CAEN_PLU_SetGateAndDelayGenerator(handle_, configuration_.Chl[i].port + 18, 1, configuration_.Chl[i].convertedGate, configuration_.Chl[i].convertedDelay, 1);
            //enabledPorts += pow(2., (configuration_.Chl[i].port));
        }
        else if (configuration_.Chl[i].type == "SPS")
        {
            //status |= CAEN_PLU_SetGateAndDelayGenerator(handle_, configuration_.Chl[i].port + 28, 1, configuration_.Chl[i].gate, configuration_.Chl[i].delay, 1);
            //enabledPorts += pow(2., (configuration_.Chl[i].port));
        }
        // Control signal feature removed
        //Except for control signal which does not go through the gdg. 
        //Instead we remember with controlidx (initialized as -1) where in the vector is the information for the control signal stored.
        //If there are more than one control signal, we just care about the first parse from the xml.
        /*else if (configuration_.Chl[i].type == "ControlSig")
        {   
            int integerdelay = round(configuration_.Chl[controlidx].delay/20.); 
            s.str(""); s << "Control signal: delay in ns " << configuration_.Chl[i].delay << " port " << configuration_.Chl[i].port << " gate (not used for control signal) " <<  configuration_.Chl[i].gate ;
            Log(s.str(),1);
            s.str(""); s <<"Delay in clock cycles" <<integerdelay;
            Log(s.str(),1);


            if (controlidx != -1) {
                s.str(""); s <<"Control signal already set, skipping..." <<integerdelay;
                Log(s.str(),1);
                continue;
            }
            controlidx = i;
            //configuration_.Chl[i].port = (configuration_.Chl[i].port + 1);  //Bit shifts to add the control port chosen signal in message
        }*/

    }
    Log("[CAEN_DT5495::Init] We don't want to open and close the connection to the board each time... Removing CloseDevice",1);
    status |= CAEN_PLU_CloseDevice(handle_);
 
    //Setting Enabled Ports, Busy Ports and Control Port
    status |= WriteReg(CAEN_DT5495_ENABLED_PORTS_REGISTER, enabledPorts);
    status |= WriteReg(CAEN_DT5495_ENABLED_BUSY_REGISTER, enabledBusy);
    
    // The register is expecting a 32 bits number where:
    // 31 - 16: stDowntime
    // 15 - 1:  stUptime
    // 0: selfTriggerEnabled
    
    uint32_t selfTriggerEnabled = configuration_.selfTriggerEnabled;
    uint32_t stDowntime = configuration_.stDowntime;
    uint32_t stUptime = configuration_.stUptime;
    //uint32_t spsSimEnabled = configuration_.spsSimEnabled;

    s.str(""); s << "CAEN_DT5495::Init values from config in ns: selfTriggerEnabled " << selfTriggerEnabled << " stDowntime " << stDowntime << " stUptime " << stUptime; Log(s.str(),1);

    stDowntime = fromNsToClockCycles(stDowntime);
    stUptime = fromNsToClockCycles(stUptime);

    // Simple check depending on the bits available to host the value
    /*if (stUptime>32767) stUptime=32767;
    if (stDowntime>65535) stUptime=65535;

    s.str(""); s << "CAEN_DT5495::Init values from config in clock cycles: selfTriggerEnabled " << selfTriggerEnabled << " stDowntime " << stDowntime << " stUptime " << stUptime; Log(s.str(),1);

    stDowntime = stDowntime << 15;
    
    s.str(""); s << "CAEN_DT5495::Init prepering register: " << stDowntime; Log(s.str(),1);

    uint32_t regValue = stDowntime + stUptime;

    s.str(""); s << "CAEN_DT5495::Init prepering register: " << regValue; Log(s.str(),1);

    regValue = regValue << 1;

    s.str(""); s << "CAEN_DT5495::Init prepering register: " << regValue; Log(s.str(),1);    

    regValue += selfTriggerEnabled;

    s.str(""); s << "CAEN_DT5495::Init prepering register: " << regValue; Log(s.str(),1);*/

    status |= WriteReg(CAEN_DT5495_SELFTRIGGER_REGISTER, selfTriggerEnabled);
    status |= WriteReg(CAEN_DT5495_SELFTRIGGER_DOWNTIME, stDowntime);
    status |= WriteReg(CAEN_DT5495_SELFTRIGGER_UPTIME, stUptime);
    //status |= WriteReg(CAEN_DT5495_SPS_GENERATOR_REGISTER, spsSimEnabled);
    
    // Setting internal busy length
    
    status |= WriteReg(CAEN_DT5495_INTERNAL_BUSY_LENGTH, fromNsToClockCycles(configuration_.internalBusyLength));

    if (status)
    {
        s.str(""); s <<"[CAEN_DT5495]::[ERROR]::Configuration error for device @ " << configuration_.IPAddress << " Status :" << status ;
        Log(s.str(),1);
        return ERR_CONFIG;;
    }

    s.str(""); s << "[CAEN_DT5495]::[INFO]::++++++ CAEN DT5495 CONFIGURED ++++++";  
    Log(s.str(),1);
    PrintReg();
    return status;

}

int CAEN_DT5495::Clear()
{
    int status=0;
    if (handle_<0)
        return ERR_CONF_NOT_FOUND;

    //Re-Initialize the Board
    return Init();
}      

int CAEN_DT5495::BufferClear()
{
    /*int status=0;
    if (handle_<0)
        return ERR_CONF_NOT_FOUND;
    //Software clear Busy
    uint32_t busyvalue = 0;
    ReadReg(CAEN_DT5495_BUSY_FLAG_REGISTER, &busyvalue);
    busyvalue = (busyvalue or 1) - 1;
    status |= WriteReg(CAEN_DT5495_BUSY_FLAG_REGISTER ,busyvalue);
    if  (status)
        return status;*/
    return 0;
}      

//Function to print values written on the registers
int CAEN_DT5495::PrintReg()
{
    int status = 0;
    for (int i = 0; i < sizeof(REGISTERS)/sizeof(REGISTERS[0]); i++)   
    {
        ostringstream s;
        uint32_t regvalue = 0;
        status |= ReadReg(REGISTERS[i], &regvalue);
        if (status)
        {
            s.str(""); s <<"[CAEN_DT5495]::[ERROR]::Communication error for device @ " << configuration_.IPAddress << " " << status ;
            Log(s.str(),1);
            return ERR_OPEN;
        }
        s.str(""); s << "[CAEN_DT5495]::[ValueWrittenOnReg]::" << std::hex << "0x" << REGISTERS[i] << ":" << "0x" << std::hex << regvalue ;
        Log(s.str(),1);
    }
    return 0;

}
int CAEN_DT5495::PrintPortsConfiguration()
{
    ostringstream s;
    s.str(""); s <<"[CAEN_DT5495]::[INFO] selfTriggerEnabled" << GetConfiguration()->selfTriggerEnabled; Log(s.str(),1);
    s.str(""); s <<"[CAEN_DT5495]::[INFO] stDowntime" << GetConfiguration()->stDowntime; Log(s.str(),1);
    s.str(""); s <<"[CAEN_DT5495]::[INFO] stUptime" << GetConfiguration()->stUptime; Log(s.str(),1);
    s.str(""); s <<"[CAEN_DT5495]::[INFO] runType" << GetConfiguration()->runType; Log(s.str(),1);
    //s.str(""); s <<"[CAEN_DT5495]::[INFO] spsSimEnabled" << GetConfiguration()->spsSimEnabled; Log(s.str(),1);

    //Print out info of channels parsed from the xml
    for (int i = 0; i < configuration_.Chl.size(); i++){
        ostringstream s;
        s.str(""); s << "[CAEN_DT5495]::[INFO]::Channels:\n"
                     << "ID: " << configuration_.Chl[i].id << "\n"
                     << "TYPE: " << configuration_.Chl[i].type << "\n" 
                     << "PORT: " << configuration_.Chl[i].port << "\n"
                     << "Gate: " << configuration_.Chl[i].gate << "\n"
                     << "Delay: " << configuration_.Chl[i].delay << "\n"
                     << "Converted Gate: " << configuration_.Chl[i].convertedGate << "\n"
                     << "Converted Delay: " << configuration_.Chl[i].convertedDelay;
        Log(s.str(),1);
    }
    return 0;
}

int CAEN_DT5495::Config(BoardConfig *bC)
{
    
    ostringstream s; s.str(""); s << "[CAEN_DT5495]::[Config] Begin of config function";
    Log(s.str(),1);
    Board::Config(bC);
    xmlNode * board_node = bC->GetBoardNodePtr ();
    //here the parsing of the xmlnode...
    GetConfiguration()->IPAddress=Configurable::getElementContent(*bC, "IPAddress", board_node);

    // Reding self trigger and divider values
    GetConfiguration()->selfTriggerEnabled = 0;
    GetConfiguration()->stDowntime = 0; // no divider
    GetConfiguration()->stUptime = 0; // no duty cycle
    //GetConfiguration()->spsSimEnabled = 0;

    // Initially this block was in a try/catch block. But if something goes wrong we really want to crash the program :)
    string tempString = Configurable::getElementContent(*bC, "selfTriggerEnabled", board_node);
    if(tempString != "NULL")
    {
        GetConfiguration()->selfTriggerEnabled = stoi(tempString);
        tempString = Configurable::getElementContent(*bC, "stDowntime", board_node);
        if(tempString != "NULL") {
            uint32_t newTriggerDividerValue = stoi(tempString);
            GetConfiguration()->stDowntime = newTriggerDividerValue;
        }
        tempString = Configurable::getElementContent(*bC, "stUptime", board_node);
        if(tempString != "NULL") {
            uint32_t newDutyCycleValue = stoi(tempString);
            GetConfiguration()->stUptime = newDutyCycleValue;
        }
  
        //here the parsing of the xmlnode for ports
        xmlNode * ch_node = NULL;	
        for (ch_node = bC->GetBoardNodePtr ()->children; ch_node ; ch_node = ch_node->next)
        {
            //Loop over channel node under the board node
            if (ch_node->type == XML_ELEMENT_NODE && xmlStrEqual (ch_node->name, xmlCharStrdup ("Channel"))  )
                ParseConfigForChannels (bC, ch_node);
        }
    }
    tempString = Configurable::getElementContent(*bC, "internalBusyLength", board_node);
    GetConfiguration()->internalBusyLength = stoi(tempString);
    //tempString = Configurable::getElementContent(*bC, "spsSimEnabled", board_node);
    //GetConfiguration()->spsSimEnabled = stoi(tempString); 
    PrintPortsConfiguration();
    return 0;
}

int CAEN_DT5495::ParseConfigForChannels (BoardConfig * bC, const xmlNode * node)
{
    int idx = configuration_.Chl.size() ;
    ostringstream s;
    string type = Configurable::getElementContent (*bC, "Type", node) ;

    //Each I/O must have a type of TriggerInput/ TriggerOuput/ SPS/ Busy
    if (type == "NULL")
    {
        s.str(""); s << "[CAEN_DT5495]::[ERROR]:: Field Type not found in board xml node config for a channel";
        Log(s.str(),1);
        return ERR_CONF_INVALID ;
    }

    idx++;
    GetConfiguration()->Chl.push_back(EnabledCh());
    GetConfiguration()->Chl[idx - 1].id = stoi(Configurable::getElementContent (*bC, "ID", node));
    GetConfiguration()->Chl[idx - 1].type = type;
    GetConfiguration()->Chl[idx - 1].port = stoi(Configurable::getElementContent (*bC, "Port", node)); 
    GetConfiguration()->Chl[idx - 1].delay = stoi(Configurable::getElementContent (*bC, "Delay", node));
    // For the moment we set convertedDelay and convertedGate to 0
    // They will be defined during Init by gdConverter function
    GetConfiguration()->Chl[idx - 1].convertedDelay = 0;
    GetConfiguration()->Chl[idx - 1].convertedGate = 0;


  
    //Gate field can be empty for inputs not going through the gdg, in this case the gate is set to zero and wont be used in the future
    string gate = Configurable::getElementContent (*bC, "Gate", node);
    if (gate != "NULL")
    {
        GetConfiguration()->Chl[idx - 1].gate = stoi(gate);
    }
    else { GetConfiguration()->Chl[idx - 1].gate = 0;}

    return 0 ;
}


//The following functions are copied from CAEN_VX718, which seems to be the trigger board used before. 
//SendSignal sends busy (pulse or constant LOW/HIGH) and trigger ack (Pluse) signals for CAEN_VX718
//Not in use for the moment

/*
  int CAEN_DT5495::SendSignal(DT5495_DAQ_Signals sig)
  {
  int status = 0;
  if (handle_<0)
  return ERR_CONF_NOT_FOUND;

  if(sig==DAQ_CLEAR_BUSY)
  status |= CAEN_PLU_WriteReg(handle_, CAEN_DT5495_BUSY_FLAG_REGISTER, 0);
  //TODO: TrigAck??
  // else if(sig==DAQ_TRIG_ACK)
  // status |= CAENVME_PulseOutputRegister(handle_,configuration_.trigAckOutputBit);
  else if(sig==DAQ_BUSY_ON)
  {
  status |= CAEN_PLU_WriteReg(handle_, CAEN_DT5495_BUSY_FLAG_REGISTER, 1);
  }
  //TODO: Any differences from CLEAR BUSY???
  else if(sig==DAQ_BUSY_OFF)
  {
  status |= CAEN_PLU_WriteReg(handle_, CAEN_DT5495_BUSY_FLAG_REGISTER, 0);
  }
  else
  return ERR_DAQ_SIGNAL_UNKNOWN;

  if (status)
  return ERR_DAQ_SIGNAL;

  return 0;
  }
*/

int CAEN_DT5495::SetBusyOff()
{
    int status = 0;
    if (handle_<0)
        return ERR_CONF_NOT_FOUND;
 
   	 
    uint32_t busyvalue = 0;
    // ostringstream s;
    // s.str(""); s << "[CAEN_DT5495]::[DEBUG] SetBusyOff before ReadReg CAEN_DT5495_BUSY_FLAG_REGISTER";
    // Log(s.str(),3);
    ReadReg(CAEN_DT5495_BUSY_FLAG_REGISTER, &busyvalue);
    busyvalue = (busyvalue or 1) - 1;
    // s.str(""); s << "[CAEN_DT5495]::[DEBUG] SetBusyOff before writereg CAEN_DT5495_BUSY_FLAG_REGISTER";
    //Log(s.str(),3);
    status |= WriteReg(CAEN_DT5495_BUSY_FLAG_REGISTER ,busyvalue);
    // s.str(""); s << "[CAEN_DT5495]::[DEBUG] SetBusyOff return status" << status;
    //Log(s.str(),3);
    if  (status)
        return status;
    return 0;
}

int CAEN_DT5495::ClearBusy()
{
    return SetBusyOff();
}

//Used by VX718 to ack trigger by sending pulses, not in use for the moment
int CAEN_DT5495::TriggerAck()
{
    /* 
       int status = 0;
       if (handle_<0)
       return ERR_CONF_NOT_FOUND;
  
       //send DAQ_TRIG_ACK
       status = SendSignal(DAQ_TRIG_ACK);
       if (status)
       return status;
    */
    return 0;
}

bool CAEN_DT5495::TriggerReceived()
{

  int status=0;
  //loop until scaler switch to 1
  //  bool trigger=false;
  uint32_t triggerflag = 0;
  status = ReadReg(CAEN_DT5495_TRIGGER_FLAG, &triggerflag);

  if (handle_<0)
    return false;

  if (status)
    return false;
  // the triggerflag is actually a counter, a new trigger has been received if triggerflag is differnt from trig_count_
  // NOTE: trig_count_ should be initialized to -1 to avoid missing the first event 
  if (triggerflag != trig_count_) {      
    clock_gettime(CLOCK_MONOTONIC_RAW, &trig_time_); //set the nsec precise trig_time
    trig_count_ = triggerflag;
    if (triggerflag>1) {  
      ostringstream s; s << "[CAEN_VX718]::[WARNING]::SCALER >1";
      Log(s.str(),1);
    }
    return true;
  } else {
    return false;
  }
  return false;
}

int CAEN_DT5495::SetBusyOn()
{ 
    int status = 0;
    if (handle_<0)
        return ERR_CONF_NOT_FOUND;
    uint32_t busyvalue = 0;
    ReadReg(CAEN_DT5495_BUSY_FLAG_REGISTER, &busyvalue);
    busyvalue = busyvalue or 1;
    status |= WriteReg(CAEN_DT5495_BUSY_FLAG_REGISTER ,busyvalue);
    if  (status)
        return status;
    return 0;
} 
  
   
//Reading nothing for the moment
int CAEN_DT5495::Read(vector<WORD> &v)
{
    return 0;
}

int CAEN_DT5495::UpdateGDG(char* new_gdg_pars){
    ostringstream s; s.str(""); s <<"[CAEN_DT5495]::[UpdateGDG] New UpdateGDG function; new_gdg_pars: " << new_gdg_pars;
    Log(s.str(),1);

    string pars_string;
    stringstream ss;
    ss << new_gdg_pars;
    ss >> pars_string;

    string delimeter = ",";
    vector<string> gdg_parts;

    size_t comma_index = 1;

    // Parse input
    while (true){
        comma_index = pars_string.find(delimeter);
        if (comma_index==-1) break;
        string token = pars_string.substr(0, comma_index); 
        if (token.size()>0){
            gdg_parts.push_back(token);
        }
        pars_string.erase(0, comma_index + delimeter.length());
    }
    gdg_parts.push_back(pars_string);
    int channel_id = 999;
    int idx=0;
    for (int i=0; i<gdg_parts.size(); i++) {

        s.str(""); s <<"[CAEN_DT5495]::[UpdateGDG] Contents of gdg_parts: " << gdg_parts[i];
        Log(s.str(),1);
        
        comma_index = gdg_parts[i].find(":");
        if (comma_index==-1) break;
        
        string key = gdg_parts[i].substr(0, comma_index);
        if (key=="selfTriggerEnabled"){
            GetConfiguration()->selfTriggerEnabled  = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
        }else if (key=="stUptime"){
            GetConfiguration()->stUptime  = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
        }else if (key=="stDowntime"){
            GetConfiguration()->stDowntime  = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
        }else if (key=="ID"){
            channel_id = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
            for (int j=0; j<GetConfiguration()->Chl.size(); j++) {
                if (channel_id == GetConfiguration()->Chl[j].id) idx=j;
            }
        /*}else if (key=="spsSimEnabled"){
            GetConfiguration()->spsSimEnabled = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));*/
        } else {
            if(key=="Type") {
                GetConfiguration()->Chl[idx].type = gdg_parts[i].substr(comma_index+1, gdg_parts[i].size());
            } else if (key=="Port") {
                GetConfiguration()->Chl[idx].port = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
            } else if (key=="Delay") {
                GetConfiguration()->Chl[idx].delay = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
            } else if (key=="Gate") {
                GetConfiguration()->Chl[idx].gate = stoi(gdg_parts[i].substr(comma_index+1, gdg_parts[i].size()));
            }
        }
        
    }  

    s.str(""); s <<"[CAEN_DT5495]::[UpdateGDG] Configuration overwritten. Printing new config..."; Log(s.str(),1);
    PrintPortsConfiguration(); 
    s.str(""); s <<"[CAEN_DT5495]::[UpdateGDG] Clearing buffers..."; Log(s.str(),1);
    BufferClear();
    s.str(""); s <<"[CAEN_DT5495]::[UpdateGDG] Reconfiguring electronics..."; Log(s.str(),1);
    int status = Init();
    s.str(""); s <<"[CAEN_DT5495]::[UpdateGDG] UpdateGDG done."; Log(s.str(),1);
    // CLEAR BUFFERS
//    BufferClear()

    // INITIALIZE
//    Init()
    return status;
}

// We expect a message like 1:234:567 where 
// 1: can be just 1 or 0, enable dissable self trigger
// 234: a number with no length define, it is the divider
// 567: a number with no length define, it is the duty cycle
int CAEN_DT5495::UpdateSelfTrigger(char* new_selftrig_pars){
    ostringstream s; s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] new_selftrig_pars: " << new_selftrig_pars;
    Log(s.str(),1);

    string pars_string;
    stringstream ss;
    ss << new_selftrig_pars;
    ss >> pars_string;
    
    size_t column_index_1, column_index_2 = 0;
    column_index_1 = pars_string.find(":");
    column_index_2 = pars_string.rfind(":");

    GetConfiguration()->selfTriggerEnabled  = stoi(pars_string.substr(0, column_index_1));
    GetConfiguration()->stDowntime      = stoi(pars_string.substr(column_index_1+1, column_index_2 - column_index_1 - 1));
    GetConfiguration()->stUptime           = stoi(pars_string.substr(column_index_2+1, pars_string.size() - column_index_2));

    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] selfTriggerEnabled" << GetConfiguration()->selfTriggerEnabled; Log(s.str(),1);
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] stDowntime" << GetConfiguration()->stDowntime; Log(s.str(),1);
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] stUptime" << GetConfiguration()->stUptime; Log(s.str(),1);

    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] Configuration overwritten. Printing new config..."; Log(s.str(),1);
    PrintPortsConfiguration(); 
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] Clearing buffers..."; Log(s.str(),1);
    BufferClear();
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] Reconfiguring electronics..."; Log(s.str(),1);
    int status = Init();
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSelfTrigger] UpdateSelfTrigger done."; Log(s.str(),1);
    return status;
}

/*int CAEN_DT5495::UpdateSpsSim(char* new_spssim_pars){
    ostringstream s; s.str(""); s <<"[CAEN_DT5495]::[UpdateSpsSim] new_spssim_pars: " << new_spssim_pars;
    Log(s.str(),1);

    string pars_string;
    stringstream ss;
    ss << new_spssim_pars;
    ss >> pars_string;
    
    size_t column_index_1, column_index_2 = 0;
    column_index_1 = pars_string.find(":");
    column_index_2 = pars_string.rfind(":");

    GetConfiguration()->spsSimEnabled  = stoi(pars_string.substr(0, column_index_1));
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSpsSim] spsSimEnabled" << GetConfiguration()->spsSimEnabled; Log(s.str(),1);

    s.str(""); s <<"[CAEN_DT5495]::[UpdateSpsSim] Configuration overwritten. Printing new config..."; Log(s.str(),1);
    PrintPortsConfiguration(); 
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSpsSim] Clearing buffers..."; Log(s.str(),1);
    BufferClear();
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSpsSim] Reconfiguring electronics..."; Log(s.str(),1);
    int status = Init();
    s.str(""); s <<"[CAEN_DT5495]::[UpdateSpsSim] UpdateSpsSimTrigger done."; Log(s.str(),1);
    return status;
}*/

int CAEN_DT5495::EnableSelfTrigger(){

    uint32_t selfTriggerEnabled = 1;
    uint32_t regValue = 0;
    ReadReg(CAEN_DT5495_SELFTRIGGER_REGISTER, &regValue);
    regValue = regValue | selfTriggerEnabled;
    int status = WriteReg(CAEN_DT5495_SELFTRIGGER_REGISTER, regValue);
    return status;
}

/*int CAEN_DT5495::EnableSpsSim(){

    uint32_t spsSimEnabled = 1;
    uint32_t regValue = 0;
    ReadReg(CAEN_DT5495_SPS_GENERATOR_REGISTER, &regValue);
    regValue = regValue | spsSimEnabled;
    int status = WriteReg(CAEN_DT5495_SPS_GENERATOR_REGISTER, regValue);
    return status;
}*/
//Function to intrepret cmd from gui to modify/enable the gdg (not tested, need future modifications to give more flexibility of what we can do from the gui)
//The cmd format should follow GUI_GDG E(or F) PORT_NUMBER GATE DELAY
/*int CAEN_DT5495::UpdateGDG(int enabled, char* port, int portnu, double gate, double delay){

    int status = 0;
    string DT_ip = configuration_.IPAddress;
    char *ip = &DT_ip[0u];
    delay = round( (delay - T_0)/T_1 + 1 );
    gate  = round( gate/T_1 + 2 ); 
    status |= CAEN_PLU_OpenDevice(CAEN_PLU_CONNECT_DIRECT_ETH, ip, 0, 0, &handle_);
    uint32_t enabledPorts;
    uint32_t enabledBusy;
    if (port == "E")
    {
        if (controlidx!=-1 & portnu == configuration_.Chl[controlidx].port)
        {
            status |= WriteReg(CAEN_DT5495_CONTROL_DELAY_REGISTER, round(delay/20.));
        }
        else
        {
            status |= CAEN_PLU_SetGateAndDelayGenerator(handle_, portnu, 1, gate, delay, 1);
        }

        status |= CAEN_PLU_ReadReg(handle_, CAEN_DT5495_ENABLED_PORTS_REGISTER, &enabledPorts);
        if (enabled)
        {
            enabledPorts |= 1 << portnu;
        }
        else
        {
            enabledPorts &= ~(1 << portnu);
        }

        status |= WriteReg(CAEN_DT5495_ENABLED_PORTS_REGISTER, enabledPorts);
  
    }
    else if (port == "F")
    {
        status |= CAEN_PLU_SetGateAndDelayGenerator(handle_, portnu + 8, 1, gate, delay, 1);
        status |= CAEN_PLU_ReadReg(handle_, CAEN_DT5495_BUSY_FLAG_REGISTER, &enabledPorts);
        if (enabled)
        {
            enabledBusy |= 1 << (portnu + 8);
        }
        else
        {
            enabledBusy &= ~(1 << (portnu + 8));
        }
        status |= WriteReg(CAEN_DT5495_BUSY_FLAG_REGISTER, enabledBusy);
    }
  
    else
    {
        ostringstream s; s.str(""); s << "[CAEN_DT5495]::[ERROR]:: Input Port should be either E or F";
        Log(s.str(),1);
        return ERR_RESET ;
    }
   
    if (status)
    {
        ostringstream s; s.str(""); s <<"[CAEN_DT5495]::[ERROR]::Unable to connect for Reconfigruation of GDG @ " << configuration_.IPAddress << " Status :" << status ;
        Log(s.str(),1);
        return ERR_RESET;
    }
  
    status |= CAEN_PLU_CloseDevice(handle_);

    return status;
}*/


//Functions copied from the old I/O controller to signal the WWE/WE/EE
//Basically nothing is changed except the mask is now hardcoded

bool CAEN_DT5495::SignalReceived(CMD_t signal)
{
    uint32_t data=999;
    ReadInput(data);

    ostringstream s; s << "[CAEN_DT5495]::[SignalReceived]::Signal " << data;
    Log(s.str(),1);

    if (signal == WWE ) 
    {
        return (data & 1);
    }
    else if (signal == WE ) 
    {
        return (data & 2);
    }
    else if (signal == EE ) 
    {
        return (data & 4);
    }
    else
    {
        ostringstream s; s << "[CAEN_DT5495]::[ERROR]::Signal " << signal << " not handled by CAEN_DT5495";
        Log(s.str(),1);
        return false;
    }
    return false;

}

int CAEN_DT5495::ReadInput(uint32_t& data)
{
    if (handle_<0)
        return ERR_CONF_NOT_FOUND;

    int status=0;
  
    status |= ReadReg(CAEN_DT5495_SPS_LAST_SIGNAL, &data);
 
    if (status)
    {
        ostringstream s; s << "[CAEN_DT5495]::[ERROR]::Cannot read I/O register " << status; 
        Log(s.str(),1);
        return ERR_READ;
    }
    return 0;
}

int CAEN_DT5495::SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus)  {

  if (triggerType==1) { //BEAM_TRG
    WriteReg(CAEN_DT5495_BUSY_FLAG_REGISTER ,!triggerStatus);
  }
  return 0;
}
