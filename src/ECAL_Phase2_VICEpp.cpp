#ifndef NO_CACTUS
#include "interface/ECAL_Phase2_VICEpp.hpp"
#include <vector>
#include <typeinfo>

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <bitset>

#include "TString.h"

//#define DEEPDEBUG 1
//#define DEBUGPAYLOAD 1
#define I2C_SHIFT_DEV_NUMBER 4
#define PWUP_RESETB            (1<<31)
#define RESET                  (1<<0)
#define NSAMPLE_MAX 26624
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays
#define DELAY_TAP_DIR   (1<<5)     // Increase (1) or decrease (0) tap delay in ISERDES
#define DRP_WRb         (1<<31)

#define INVERT_RESYNC          (1<<3)
#define LVRB_AUTOSCAN          (1<<2)
#define PED_MUX                (1<<13)

#define eLINK_ACTIVE        (1<<8)
#define ADC_CALIB_MODE      (1<<0)
#define ADC_TEST_MODE       (1<<1)
#define ADC_MEM_MODE        (1<<5)

#define I2C_TOGGLE_SDA         (1<<31)
#define RESYNC_IDLE_PATTERN (1<<0)
#define ADC_INVERT_DATA     (1<<8)
#define RESYNC_PHASE           (1<<16)

#define LiTEDTU_stop              0
#define LiTEDTU_start             1
#define LiTEDTU_DTU_reset         2
#define LiTEDTU_I2C_reset         3
#define LiTEDTU_ADCTestUnit_reset 4
#define LiTEDTU_SYNC_MODE         5
#define LiTEDTU_NORMAL_MODE       6
#define LiTEDTU_FLUSH             7
#define LiTEDTU_ADCH_reset        8
#define LiTEDTU_ADCH_calib        9
#define LiTEDTU_ADCL_reset        10
#define LiTEDTU_ADCL_calib        11
#define LiTEDTU_CATIA_TP          13
#define LiTEDTU_BC0_MARKER        14

#define SELF_TRIGGER_MODE      (1<<31)
#define SELF_TRIGGER_MASK      (1<<22)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER           (1<<3)
#define SOFT_TRIGGER           (1<<2)
#define TRIGGER_MODE           (1<<1)

#define CAPTURE_START 1
#define CAPTURE_STOP  2

#define SW_DAQ_DELAY (1<<16)  // delay for laser with external trigger
#define HW_DAQ_DELAY (1<<0)   // Laser with external trigger

#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_100HZ             (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)

#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define I2C_CATIA_TYPE   1
#define I2C_LiTEDTU_TYPE 2

#define DELAY_AUTO_TUNE (1<<29)    // Launch autamatic IDELAY tuning to get the eye center of each stream

#define ERR_CONF_INVALID 1
#define I2C_DTU_NREG 20

int ECAL_Phase2_VICEpp::Init()
{
    Log("[ECAL_Phase2_VICEpp::Init] entering...", 1);
    uhal::ConnectionManager manager (_manager_cfg);
    for (auto & d : _devices) {
        _dv.push_back(manager.getDevice(d));
    }

    try {
        //Reset(); // Reset performed in ConfigFromNewCode
        ConfigFromNewCode();
    } catch (std::exception &e ) {
        Log("[ECAL_Phase2_VICEpp::Init] Error during ConfigFromNewCode", 3);
    }
    for (auto & hw : _dv) {
        Log("[ECAL_Phase2_VICEpp::Init] FW version loop", 1);
        // Read FW version to check :
        _fw_version = hw.getNode("FW_VER").read();
        hw.dispatch();        
        Log(Form("[ECAL_Phase2_VICEpp::Init] FW version %x", _fw_version.value()), 1);
    }

    // init number of words needed for transfer
    int n_word = (_nsamples + 1) * 6; // 3*32 bits words per sample to get the 5 channels data
    _n_transfer = n_word / (ECAL_Phase2_VICEpp_MAX_PAYLOAD / 4); // max ethernet packet = 1536 bytes, max user payload = 1500 bytes
    _n_last = n_word - _n_transfer * (ECAL_Phase2_VICEpp_MAX_PAYLOAD / 4);
    Log(Form("[ECAL_Phase2_VICEpp::Init] Reading events by blocks of %dx32b-words, %d bits", n_word, n_word * 32), 1);
    Log(Form("[ECAL_Phase2_VICEpp::Init] Using %d transfers of %d words + 1 transfer of %d words", _n_transfer, ECAL_Phase2_VICEpp_MAX_PAYLOAD / 4, _n_last), 1);
    if(_n_transfer >= 248)
    {
        Log("[ECAL_Phase2_VICEpp::Init] Event size too big! Please reduce number of samples per frame.", 1);
        Log("[ECAL_Phase2_VICEpp::Init] Max frame size : 28670", 1);
    }
    //int reset = StartDAQ() && StopDAQ();
    StartDAQ();
    StopDAQ();
    Print();

    // init event header
    //
    // if different FW versions across devices, the version of the last device is written
    _header=0;
    setHeadFwVersion(_fw_version); 
    setHeadNSamples(_nsamples);
    setHeadNDevices(_dv.size());
    setHeadFrequency(3); // FIXME: if FW changed to handle different frequency, implement a function to read it from the FW directly

    _reset_fifo_cnt = 0;
    
    Log(Form("[ECAL_Phase2_VICEpp::Init] Header: %x", _header), 1);
    Log("[ECAL_Phase2_VICEpp::Init] ...returning.", 1);    
    return 0;
}


int ECAL_Phase2_VICEpp::Clear()
{
    if (_debug) Log("[ECAL_Phase2_VICEpp::Clear] entering...", 3);
    int ret = BufferClear();
    if (_debug) Log("[ECAL_Phase2_VICEpp::Clear] ...returning.", 3);
    return ret;
}

// This function should work also for new VICE version
// I have just added "hw.getNode("CAP_ADDRESS").write(0);"
// not sure it is really needed
int ECAL_Phase2_VICEpp::StartDAQ()
{

    Log("[ECAL_Phase2_VICEpp::StartDAQ] entering...", 3);
    if (_debug) Log("[ECAL_Phase2_VICEpp::StartDAQ] entering...", 3);
    // also reset memory
    for (auto & hw : _dv) {
        hw.getNode("CAP_ADDRESS").write(0);
        int command = ((_nsamples + 1)<<16) + ECAL_Phase2_VICEpp_CAPTURE_START;
        hw.getNode("CAP_CTRL").write(command);
        Log(Form("[ECAL_Phase2_VICEpp::StartDAQ] writing memory reset command for VFE %s", hw.id().c_str()), 3);
        hw.dispatch();
        Log(Form("[ECAL_Phase2_VICEpp::StartDAQ] memory reset dispatched for VFE %s", hw.id().c_str()), 3);

        ///// FIXME - unsure this is needed
        ///command = ECAL_Phase2_VICEpp_DAC_WRITE | ECAL_Phase2_VICEpp_DAC_VAL_REG | (_calib_level&0xffff);
        ///hw.getNode("VFE_CTRL").write(command);
        ///hw.dispatch();
        ///if (_debug) Log(Form("[ECAL_Phase2_VICEpp::StartDAQ] calib level set for VFE %s", hw.id().c_str()), 3);
    }
    Log("[ECAL_Phase2_VICEpp::StartDAQ] ...returning.", 3);
    return 0;
}

// This function should work also for new VICE version
int ECAL_Phase2_VICEpp::StopDAQ()
{
    if (_debug) Log("[ECAL_Phase2_VICEpp::StopDAQ] entering...", 3);
    for (auto & hw : _dv) {
        int command = ((_nsamples + 1)<<16) + ECAL_Phase2_VICEpp_CAPTURE_STOP;
        hw.getNode("CAP_CTRL").write(command);
        hw.dispatch();
    }
    if (_debug) Log("[ECAL_Phase2_VICEpp::StopDAQ] ...returning.", 3);
    return 0;
}


int ECAL_Phase2_VICEpp::Reset()
{
    if (_debug) Log("[ECAL_Phase2_VICEpp::Reset] entering...", 3);
    for (auto & hw : _dv) {
        powerUpReset(hw);
        VFEReset(hw);
        IOReset(hw);
    }
    usleep(5000000);
    if (_debug) Log("[ECAL_Phase2_VICEpp::Reset] ...returning.", 3);
    return 0;
}

int ECAL_Phase2_VICEpp::ConfigFromNewCode() {

    UInt_t command              = 0;   
    // VFE_CTRL parameters
    int invert_Resync           = 0;
    int LVRB_autoscan           = 0;
    int ped_mux                 = 0; // Force mux to be (1) or not (0) in DAC position
                                     // Set to 0 in Marc example
    int eLink_active            = 0x1f; // Value used in Marc example but it is better to get it from xml config
    Int_t ADC_calib_mode        = 0;
    Int_t ADC_test_mode         = 0;
    Int_t ADC_MEM_mode          = 0;
   
    // RESYNC_IDLE parameters 
    int I2C_toggle_SDA          = 0;
    UInt_t Resync_idle_pattern  = 0x66;
    Int_t ADC_invert_data       = 0;

    int resync_phase            = 0;
    Int_t VICEPP_clk            = 1; // 0 = Use local oscillator for all clocks 
                                     // 1 = Transmit clock fro FE board to VFE 
    Int_t ADC_invert_clk        = 0;
    Int_t enable_40MHz_out      = 0;
    // VICE_CTRL parameters

    Int_t iret;

    Int_t TP_delay              = 50;
    TP_width_                    = 16;
    //if(TP_level_ > 0) TP_width_  = 10;

    Int_t seq_clock_phase       = 0;   // sequence clock phase by steps of 45 degrees
    Int_t IO_clock_phase        = 0;   // Capture clock phase by steps of 45 degrees
    Int_t reg_clock_phase       = 0;   // Shift register clock phase by steps of 45 degrees
    Int_t mem_clock_phase       = 0;   // memory clock phase by steps of 45 degrees
    Int_t old_read_address      = 0;

    uhal::ValWord<uint32_t> delays;
    uhal::ValWord<uint32_t> address;
    uhal::ValWord<uint32_t> free_mem;
    uhal::ValWord<uint32_t> trig_reg;
    uhal::ValWord<uint32_t> reg;

    Int_t I2C_LiTEDTU_type      = I2C_LiTEDTU_TYPE;
    Int_t I2C_shift_dev_number  = I2C_SHIFT_DEV_NUMBER;
    Int_t delay_auto_tune       = 1;

    // This block corresponds to the request of "reset_all" in Marc example
    Int_t IO_reset              = 1;
    Int_t pwup_reset            = 1;
    Int_t init_DTU              = 1;
    Int_t calib_ADC             = 3;

    Int_t CATIA_version[5]     = {13};

    Int_t delay_val[5];    // Number of delay taps to get a stable R/O with ADCs (160 MHz)
    Int_t bitslip_val[5];  // Number of bit to slip with iserdes lines to get ADC values well aligned
    Int_t byteslip_val[5]; // Number of byte to slip to aligned ADC data on 40 MHz clock

    Int_t I2C_CATIA_type = I2C_CATIA_TYPE;
    Int_t dither = 0;
    Int_t nsample_calib_x4 = 0;
    Int_t global_test = 0;
    
    for (auto & hw : _dv) {
        
        // It seems we don't need to take these values from the xml anymore. We set a value here followinf Marc example
        DTU_bulk1_ = 0x0007038F;
        DTU_bulk2_ = 0x88000000;
        DTU_bulk3_ = 0x00553040;
        DTU_bulk4_ = 0x65040000;
        DTU_bulk5_ = 0x000FFF3C;

        DTU_bulk1_=(DTU_bulk1_&0xffff2fbf) | (ADC_invert_clk<<12) | ((DTU_force_G1_*3)<<14)  | ((DTU_force_G10_*1)<<14) | (enable_40MHz_out<<6);
        if(ADC_test_mode==0) DTU_bulk1_&=0xfffffff1;
        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Enabling 40 MHz clock : %d, 0x%8.8x\n",enable_40MHz_out, DTU_bulk1_), 3);

        for(int ich=0; ich<5; ich++) {
            delay_val[ich]=0; 
            bitslip_val[ich]=0;  
            byteslip_val[ich]=0; 
        }

        unsigned int VFE_control=
            INVERT_RESYNC*invert_Resync |
            LVRB_AUTOSCAN*LVRB_autoscan |
            PED_MUX*ped_mux |
            eLINK_ACTIVE*eLink_active |
            ADC_CALIB_MODE*ADC_calib_mode |
            ADC_TEST_MODE*ADC_test_mode |
            ADC_MEM_MODE*ADC_MEM_mode;

        if(VICEPP_clk >=0) {
            reg = hw.getNode("VICEPP_CLK").read();
            hw.dispatch();
            printf("Clock configuration aleady present : 0x%x\n",reg.value());
            if((reg.value()&3) == (VICEPP_clk&3)) {    
                printf("Present XPoint setting 0x%8.8x already conform to request 0x%8.8x\n",reg.value(),VICEPP_clk);
                printf("Do nothing !\n");
            } else {    
                printf("Present XPoint setting 0x%8.8x not conform to request 0x%8.8x\n",reg.value(),VICEPP_clk);
                printf("Set clock configuration for VICE++ board : 0x%8.8x\n",VICEPP_clk);
                hw.getNode("VICEPP_CLK").write(VICEPP_clk);
                hw.dispatch();
                usleep(100000);
                pwup_reset = 1; // Force LiTE-DTU reset if clock has changed
            }    
        }

        // Time to do some reset/initialization
        if(pwup_reset==1) {
            powerUpReset(hw);
        }

        // Put DTU resync in reset mode, stop uLVRB autoscan mod
        hw.getNode("VFE_CTRL").write(VFE_control);
        hw.getNode("RESYNC_IDLE").write(I2C_toggle_SDA*I2C_TOGGLE_SDA  | RESYNC_IDLE_PATTERN*Resync_idle_pattern | ADC_INVERT_DATA*ADC_invert_data );
        hw.getNode("CLK_SETTING").write(resync_phase*RESYNC_PHASE);
        hw.dispatch();
        usleep(200);

        if (init_DTU==1) {
            initDTU(hw);
        }
        // Put VICE outputs with idle patterns :
        hw.getNode("OUTPUT_CTRL").write(0);
        hw.dispatch();

        // TP trigger setting :
        command=(TP_delay<<16) | (TP_width_&0xffff);
        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] TP trigger with %d clocks width and %d clocks delay : %x",TP_width_,TP_delay,command), 3);
        hw.getNode("CALIB_CTRL").write(command);
        hw.dispatch();

        // Init stage :
        // Read FW version to check :
        reg = hw.getNode("FW_VER").read();
        // Switch to triggered mode + external trigger :
        command=
            SELF_TRIGGER_MODE     *_trigger_self_mode                |
            (SELF_TRIGGER_MASK     *(_trigger_self_mask&0x1F))        |
            (SELF_TRIGGER_THRESHOLD*(_trigger_self_threshold&0xFFF))  |
            SELF_TRIGGER          *_trigger_self                     |
            SOFT_TRIGGER          *_trigger_soft                     |
            TRIGGER_MODE          *1                                | // Always DAQ on trigger
            RESET                 *0;
        hw.getNode("VICE_CTRL").write(command);

        // Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
        command = ((_nsamples+1)<<16)+CAPTURE_STOP;
        hw.getNode("CAP_CTRL").write(command);
        // Add laser latency before catching data ~ 40 us
        hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*_sw_daq_delay | HW_DAQ_DELAY*_hw_daq_delay);
        // Switch off FE-adapter LEDs
        command = TP_MODE*0+LED_ON*0+GENE_100HZ*0+GENE_TP*0+GENE_TRIGGER*0;
        hw.getNode("FW_VER").write(command);
        hw.dispatch();
        // Set the clock phases
        command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
        hw.getNode("CLK_SETTING").write(command);
        hw.dispatch();

        // Reset the reading base address :
        hw.getNode("CAP_ADDRESS").write(0);
        // Start DAQ :
        command = ((_nsamples+1)<<16)+CAPTURE_START;
        hw.getNode("CAP_CTRL").write(command);
        // Read back delay values :
        delays=hw.getNode("TRIG_DELAY").read();
        // Read back the read/write base address
        address = hw.getNode("CAP_ADDRESS").read();
        free_mem = hw.getNode("CAP_FREE").read();
        trig_reg = hw.getNode("VICE_CTRL").read();
        hw.dispatch();

        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Firmware version      : %8.8x",reg.value()), 3);
        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Delays                : %8.8x\n",delays.value()), 3);
        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Initial R/W addresses : 0x%8.8x\n", address.value()), 3);
        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Free memory           : 0x%8.8x\n", free_mem.value()), 3);
        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Trigger mode          : 0x%8.8x\n", trig_reg.value()), 3);
  
        old_read_address=address&0xffff;
        if(old_read_address==NSAMPLE_MAX-1)old_read_address=-1;

        if(init_DTU==1) {
            // Surprisingly, when we load LitE-DTU registers, we have a calibration process. So put ped-mux in position :
            VFE_control =
                DELAY_AUTO_TUNE*delay_auto_tune |
                INVERT_RESYNC*invert_Resync |
                LVRB_AUTOSCAN*LVRB_autoscan |
                PED_MUX*1 |
                eLINK_ACTIVE*eLink_active |
                ADC_CALIB_MODE*ADC_calib_mode |
                ADC_TEST_MODE*ADC_test_mode |
                ADC_MEM_MODE*ADC_MEM_mode;
            hw.getNode("VFE_CTRL").write(VFE_control);
            hw.dispatch();
            usleep(100000); // Let some time for calibration voltages to stabilize

            unsigned int device_number, val;
            for(int ich=0; ich<5; ich++) {
                Int_t data;
                device_number=I2C_LiTEDTU_type*1000+((ich+1)<<4)+2;      // DTU sub-address
                Log(Form("Device number : %d 0x%x\n",device_number,device_number), 3);
                for(Int_t ireg=0; ireg<I2C_DTU_NREG; ireg++) {
                    if     (ireg<4) data=(DTU_bulk1_>>((ireg-0)*8))&0xFF;
                    else if(ireg<8) data=(DTU_bulk2_>>((ireg-4)*8))&0xFF;
                    else if(ireg<12)data=(DTU_bulk3_>>((ireg-8)*8))&0xFF;
                    else if(ireg<16)data=(DTU_bulk4_>>((ireg-12)*8))&0xFF;
                    else            data=(DTU_bulk5_>>((ireg-16)*8))&0xFF;
                    if(ireg==5) data=baseline_G10_[ich];
                    if(ireg==6) data=baseline_G1_[ich];
                    iret=I2C_RW(hw, device_number, ireg, data, 0, 3, _debug);
                }
                data=0xff0-baseline_G10_[ich]; // switch gain at 4090 - substrated baseline
                iret=I2C_RW(hw, device_number, 17, data&0xff, 0, 1, _debug);
                iret=I2C_RW(hw, device_number, 18, (data>>8)&0xf, 0, 1, _debug);
            }
            VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan |
                PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
                ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
            hw.getNode("VFE_CTRL").write(VFE_control);
            hw.dispatch();
            usleep(100000); // Let some time for calibration voltages to stabilize
        } else {
            unsigned int device_number;
            for(int ich=0; ich<5; ich++) {
                Int_t data;
                device_number=I2C_LiTEDTU_type*1000+((ich+1)<<4)+2;      // DTU sub-address
                data=baseline_G10_[ich];
                //printf("Setting G10 baseline substraction for channel %d to 0x%x\n",ich,data);
                iret=I2C_RW(hw, device_number, 5, data, 0, 1, _debug);
                data=baseline_G1_[ich];
                //printf("Setting G1 baseline substraction for channel %d to 0x%x\n",ich,data);
                iret=I2C_RW(hw, device_number, 6, data, 0, 1, _debug);
                data=0xff0-baseline_G10_[ich]; // switch gain at 4090 - substrated baseline
                //printf("Setting G10/G1 gain switching level for channel %d to 0x%x\n",ich,data);
                iret=I2C_RW(hw, device_number, 17, data&0xff, 0, 1, _debug);
                iret=I2C_RW(hw, device_number, 18, (data>>8)&0xf, 0, 1, _debug);
            }
        }

        // Once the PLL is set, reset ISERDES instances and get ready for synchronization
        if(IO_reset==1) {
            IOReset(hw);
        }
        Log("[ECAL_Phase2_VICEpp::ConfigFromNewCode] After init_DTU", 3);
        if(calib_ADC>0) {
            if(delay_auto_tune==1) {    
                hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
                hw.dispatch();
            }
            // Set CATIA output mux for calibation :
            // and read Vref value with XADC.
            // First : disconnect temp measurement
            // Next : Set Output mux
            // Then : Set Vref_mux for measurement

            // Read XADC register 0x40 and set the requested average to 1 in XADC
            command=DRP_WRb*0 | (0x40<<16);
            hw.getNode("DRP_XADC").write(command);
	    uhal::ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
            hw.dispatch();
            unsigned loc_ave=ave.value()&0xffff;
            Log(Form("Old config register 0x40 content : %x\n", loc_ave), 3);

            loc_ave=0x8000;
            command=DRP_WRb*1 | (0x40<<16) | loc_ave;
            hw.getNode("DRP_XADC").write(command);
            hw.dispatch();
            command=DRP_WRb*0 | (0x40<<16);
            hw.getNode("DRP_XADC").write(command);
            ave  = hw.getNode("DRP_XADC").read();
            hw.dispatch();
            loc_ave=ave.value()&0xffff;
            Log(Form("New config register 0x40 content : %x\n",loc_ave), 3);
            command=DRP_WRb*1 | (0x42<<16) | 0x0400;
            hw.getNode("DRP_XADC").write(command);
            hw.dispatch();

            for(int ich=0; ich<5; ich++) {    
                unsigned int device_number, val; 
                device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
                if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;

                // SEU auto correction, no Temp output
                val=I2C_RW(hw, device_number, 1, CATIA_Reg1_def_, 0, 3, _debug);
                Log(Form("Put CATIA %d Reg1 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg1_def_,val), 3);
            }    

            for(int ich=0; ich<5; ich++) {    
                unsigned int device_number, val; 
                device_number=I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+3;
                if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_type*1000+((ich+1)<<I2C_shift_dev_number)+0xb;

                if(CATIA_version[ich]>=14) {    
                    // present Vref to XADC
                    hw.getNode("VREF_MUX_CTRL").write(1<<ich);
                    hw.dispatch();
                    usleep(10000); // Let some time for calibration voltages to stabilize
                    // And read Vref value on temp line :
                    Int_t XADC_Temp=0x11;
		    uhal::ValWord<uint32_t> temp; 
                    Int_t average=1024,n_Vref=15;
                    printf("CATIA %d : default Vref value from XADC = ",ich+1);
                    Double_t Vref_best=0., dVref=99999.;
                    Int_t DAC_Vref_best=0;
                    for(int imeas=0; imeas<n_Vref; imeas++) {
                        double ave_val=0.;
                        Int_t loc_meas=imeas;
                        if(imeas>0)loc_meas=imeas+1;
                        command= (loc_meas<<4) | 0xF; 
                        val=I2C_RW(hw, device_number, 6, command,0, 3, 0);
                        usleep(1000);

                        for(int iave=0; iave<average; iave++) {
                            command=DRP_WRb*0 | (XADC_Temp<<16);
                            hw.getNode("DRP_XADC").write(command);
                            temp  = hw.getNode("DRP_XADC").read();
                            hw.dispatch();
                            double loc_val=double((temp.value()&0xffff)>>4)/4096.;
                            ave_val+=loc_val;
                        }
                        ave_val/=average;
                        if(fabs(ave_val-1.000)<dVref) {
                            dVref=fabs(ave_val-1.000);
                            DAC_Vref_best=imeas;
                            Vref_best=ave_val;
                        }
                        Log(Form(" %.2f",ave_val*1000.), 3);
                    }
                    Log(Form(" mV, best=%.2f, DAC=%d\n",Vref_best*1000.,DAC_Vref_best), 3);
                    command= (DAC_Vref_best<<4) | 0xF;
                    val=I2C_RW(hw, device_number, 6, command,0, 3, 0);
                }
            }

            // Ask for 4 times more calibration samples for ADCs
            VFE_control=
                DELAY_AUTO_TUNE*delay_auto_tune |
                INVERT_RESYNC*invert_Resync |
                LVRB_AUTOSCAN*LVRB_autoscan |
                PED_MUX*1 |
                eLINK_ACTIVE*eLink_active |
                ADC_CALIB_MODE*ADC_calib_mode |
                ADC_TEST_MODE*ADC_test_mode |
                ADC_MEM_MODE*ADC_MEM_mode;
            hw.getNode("VFE_CTRL").write(VFE_control);
            hw.dispatch();
            usleep(100000); // Let some time for calibration voltages to stabilize
            for(int ich=0; ich<5; ich++) {
                unsigned int device_number, val;
                device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number);
                if(dither==1) {
                    val=I2C_RW(hw, device_number+0, 3, 0x01, 0, 1, _debug);
                    val=I2C_RW(hw, device_number+1, 3, 0x01, 0, 1, _debug);
                }
                if(nsample_calib_x4==1) {
                    val=I2C_RW(hw, device_number+0, 1, 0xfe, 0, 1, _debug);
                    val=I2C_RW(hw, device_number+1, 1, 0xfe, 0, 1, _debug);
                }
                if(global_test==1) {
                    val=I2C_RW(hw, device_number+0, 0, 0x01, 0, 1, _debug);
                    val=I2C_RW(hw, device_number+1, 0, 0x01, 0, 1, _debug);
                }
            }
            Log("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Launch ADC calibration !", 3);
            hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_reset<<8) | LiTEDTU_ADCH_reset);
            hw.dispatch();
            hw.getNode("DTU_RESYNC").write((LiTEDTU_ADCL_calib<<8) | LiTEDTU_ADCH_calib);
            hw.dispatch();
            usleep(10000);
        }
        
        // Reset Test unit to get samples in right order :
        hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
        hw.dispatch();

        // Restore CAL mux in normal position after calibration, 2149 marc file
        VFE_control= DELAY_AUTO_TUNE*delay_auto_tune | INVERT_RESYNC*invert_Resync | LVRB_AUTOSCAN*LVRB_autoscan |
            PED_MUX*ped_mux | eLINK_ACTIVE*eLink_active |
            ADC_CALIB_MODE*ADC_calib_mode | ADC_TEST_MODE*ADC_test_mode | ADC_MEM_MODE*ADC_MEM_mode;
        hw.getNode("VFE_CTRL").write(VFE_control);
        hw.dispatch();
        // And wait for volateg stabilization
        usleep(100000);

        // Program CATIA according to wishes :
        // Setup default values for CATIA:
        configureCATIA(hw);
   
        // Configure DTU register 1 gain selection:
	// 00 -> window 8
	// 01 -> window 16
	// 10 -> force G10
	// 11 -> force G1
        if(DTU_force_G1_==1) Log("Force DTU output with G1 samples", 3);
        else if(DTU_force_G10_==1) Log("Force DTU output with G10 samples", 3);
        else Log("Read data in DTU free gain mode", 3);
        for(int ich=0; ich<5; ich++) {
	    Int_t device_number=I2C_LiTEDTU_type*1000+((ich+1)<<I2C_shift_dev_number)+2;
            iret=I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
            iret&=0xFF;
            if(DTU_force_G1_==1) {
                iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0xc0, 0, 1, 0);
            } else if(DTU_force_G10_==1) {
                iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0x40, 0, 1, 0);
            } else if(DTU_window_length_==1){
                iret=I2C_RW(hw, device_number, 1, (iret&0x3F)|0x80, 0, 1, 0);		
            } else {
                iret=I2C_RW(hw, device_number, 1, (iret&0x3F), 0, 1, 0);
	    }
        }
    
        // Program TP-DAC for this step
        for(int ich=0; ich<5; ich++) {
            unsigned int device_number = I2C_CATIA_TYPE*1000+((ich+1)<<I2C_shift_dev_number)+3;
            if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_TYPE*1000+((ich+1)<<I2C_shift_dev_number)+0xb;
            unsigned int val;
            if(TP_level_>=0) {
                val=I2C_RW(hw, device_number, 4, (CATIA_Reg4_def_&0xf000) | (TP_level_&0xfff),1, 3, _debug);
                Log(Form("Put %d in DAC register : 0x%x",TP_level_,val),3);
            } else {
                // switch off all the injection system
                val=I2C_RW(hw, device_number, 4, 0x0,1, 3, _debug);
            }
        }

        // Wait for Vdac to stabilize :
        usleep(500000);
  
        uhal::ValWord<uint32_t> tp_reg;
        tp_reg = hw.getNode("CALIB_CTRL").read();
        hw.dispatch();

        Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Are TP active? %x", tp_reg.value()), 3);

	Int_t device_number=I2C_LiTEDTU_type*1000+((0+1)<<I2C_shift_dev_number)+2;
	auto iret = I2C_RW(hw, device_number, 1, 0, 0, 2, 0);
	Log("[ECAL_Phase2_VICEpp::ConfigFromNewCode] DTU gain: "+std::to_string(iret), 3);
    } // End of "for (auto & hw : _dv)" 
    Log("[ECAL_Phase2_VICEpp::ConfigFromNewCode] End", 3); 
    return 0;
}

// Hard reset
void ECAL_Phase2_VICEpp::powerUpReset(uhal::HwInterface &my_hw) {
    Log("[ECAL_Phase2_VICEpp::powerUpReset] Generate PowerUp reset", 3);
    my_hw.getNode("VFE_CTRL").write(1*PWUP_RESETB);
    my_hw.getNode("VFE_CTRL").write(0*PWUP_RESETB);
    my_hw.dispatch();
    usleep(10000);
}

// Soft reset
void ECAL_Phase2_VICEpp::VFEReset(uhal::HwInterface &my_hw) {
    Log("[ECAL_Phase2_VICEpp::VFEReset] Generate Warm reset", 3);
    my_hw.getNode("VICE_CTRL").write(RESET*1);
    my_hw.getNode("VICE_CTRL").write(RESET*0);
    my_hw.dispatch();
}

void ECAL_Phase2_VICEpp::IOReset(uhal::HwInterface &my_hw) {
    Log("[ECAL_Phase2_VICEpp::IOReset] Get lock status of IDELAY input stages", 3);
    my_hw.getNode("DELAY_CTRL").write(DELAY_TAP_DIR*1 | 1*DELAY_RESET);
    uhal::ValWord<uint32_t> reg = my_hw.getNode("DELAY_CTRL").read();
    my_hw.dispatch();
    usleep(1000);
    Log(Form("[ECAL_Phase2_VICEpp::IOReset] Value read : 0x%x", reg.value()), 3);
}

void ECAL_Phase2_VICEpp::configureDTU(uhal::HwInterface &my_hw) {
    Log("[ECAL_Phase2_VICEpp::configureDTU] Configuring DTU start...", 3);
    unsigned int device_number;
    for(int ich=0; ich<5; ich++) {
        device_number=I2C_LiTEDTU_TYPE*1000+((ich+1)<<4)+2;      // DTU sub-address
        Log(Form("Device number : %d 0x%x\n",device_number,device_number), 3);
        for(Int_t ireg=0; ireg<I2C_DTU_NREG; ireg++) {
            Int_t data;
            if     (ireg<4) data=(DTU_bulk1_>>((ireg-0)*8))&0xFF;
            else if(ireg<8) data=(DTU_bulk2_>>((ireg-4)*8))&0xFF;
            else if(ireg<12)data=(DTU_bulk3_>>((ireg-8)*8))&0xFF;
            else if(ireg<16)data=(DTU_bulk4_>>((ireg-12)*8))&0xFF;
            else            data=(DTU_bulk5_>>((ireg-16)*8))&0xFF;
            if(ireg==5) data=baseline_G10_[ich];
            if(ireg==6) data=baseline_G1_[ich];
            int iret=I2C_RW(my_hw, device_number, ireg, data, 0, 3, _debug);
            data=0xff0-baseline_G10_[ich]; // switch gain at 4090 - substrated baseline
            iret=I2C_RW(my_hw, device_number, 17, data&0xff, 0, 1, _debug);
            iret=I2C_RW(my_hw, device_number, 18, (data>>8)&0xf, 0, 1, _debug);
        }
    }
    Log("[ECAL_Phase2_VICEpp::configureDTU] Configuring DTU end...", 3);
}

void ECAL_Phase2_VICEpp::initDTU(uhal::HwInterface &my_hw) {
    Log("[ECAL_Phase2_VICEpp::initDTU] Prepare LiTEDTU for safe running (generate ReSync start sequence)", 3);
    // DTU Resync init sequence:
    my_hw.getNode("DTU_RESYNC").write(LiTEDTU_DTU_reset);
    my_hw.dispatch();
    my_hw.getNode("DTU_RESYNC").write(LiTEDTU_I2C_reset);
    my_hw.dispatch();
    my_hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCTestUnit_reset);
    my_hw.dispatch();
    my_hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCH_reset);
    my_hw.dispatch();
    my_hw.getNode("DTU_RESYNC").write(LiTEDTU_ADCL_reset);
    my_hw.dispatch();
    my_hw.getNode("DTU_BULK1").write(DTU_bulk1_);
    my_hw.getNode("DTU_BULK2").write(DTU_bulk2_);
    DTU_bulk3_=(DTU_bulk3_&0xffff00fe)|((pll_conf_[2]&0xFF)<<8)|((pll_conf_[2]&0x100)>>8);
    my_hw.getNode("DTU_BULK3").write(DTU_bulk3_);
    my_hw.getNode("DTU_BULK4").write(DTU_bulk4_);
    my_hw.getNode("DTU_BULK5").write(DTU_bulk5_);
    my_hw.dispatch();
    usleep(200);
}

int ECAL_Phase2_VICEpp::BufferClear()
{
    if (_debug) Log("[ECAL_Phase2_VICEpp::BufferClear] entering..." ,3);
    int ret = !(!StopDAQ() && !StartDAQ());
    if (_debug) Log("[ECAL_Phase2_VICEpp::BufferClear] ...returning.", 3);
    return ret;
}


int ECAL_Phase2_VICEpp::ClearBusy()
{  
    if (_debug) Log("[ECAL_Phase2_VICEpp::ClearBusy] entering...", 3);
    //int ret = BufferClear();
    if (_debug) Log("[ECAL_Phase2_VICEpp::ClearBusy] ...returning.", 3);
    return 0;
}


int ECAL_Phase2_VICEpp::Config(BoardConfig * bc)
{
    Log("[ECAL_Phase2_VICEpp::Config] entering...", 1);
    Board::Config(bc);
    ParseConfiguration(bc);
    Log("[ECAL_Phase2_VICEpp::Config] ...returning.", 1);
    return 0;
}

void ECAL_Phase2_VICEpp::configureCATIA(uhal::HwInterface &my_hw) {
    Log("[ECAL_Phase2_VICEpp::configureCATIA] Configuring CATIA start...", 3);
    try {
        for(int ich=0; ich<5; ich++) {
            unsigned int val;
            unsigned int I2C_shift_dev_number = I2C_SHIFT_DEV_NUMBER;
            unsigned int device_number = I2C_CATIA_TYPE*1000+((ich+1)<<I2C_shift_dev_number)+0x3;
            if(ich==4 && I2C_shift_dev_number==4)   device_number = I2C_CATIA_TYPE*1000+((ich+1)<<I2C_shift_dev_number)+0xb;

            // SEU auto correction, no Temp output
            val=I2C_RW(my_hw, device_number, 1, CATIA_Reg1_def_, 0, 3, _debug);
            Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Put CATIA %d Reg1 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg1_def_, val), 3);

            // Gain 400 Ohm, Output stage for 1.2V, LPF35, 0 pedestal
            val=I2C_RW(my_hw, device_number, 3, CATIA_Reg3_def_[ich], 1, 3, _debug);
            Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Put CATIA %d Reg3 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg3_def_[ich], val), 3);

            // DAC1 0, DAC2 0, DAC1 ON, DAC2 OFF, DAC1 copied on DAC2
            val=I2C_RW(my_hw, device_number, 4, CATIA_Reg4_def_, 1, 3, _debug);
            Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Put CATIA %d Reg4 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg4_def_, val), 3);

            // DAC2 mid scale but OFF, so should not matter
            //val=I2C_RW(hw, device_number, 5, 0xffff, 1, 3, debug);
            // DAC2 at 0 but OFF, so should not matter
            val=I2C_RW(my_hw, device_number, 5, CATIA_Reg5_def_, 1, 3, _debug);
            Log(Form("[ECAL_Phase2_VICEpp::ConfigFromNewCode] Put CATIA %d Reg5 content to 0x%x : 0x%x\n",ich+1,CATIA_Reg5_def_, val), 3);

            CATIA_Reg6_def_=0x0 | (TP_gain_<<2);

            val=I2C_RW(my_hw, device_number, 6, CATIA_Reg6_def_, 0, 3, _debug);
            Log(Form("Put CATIA %d Reg6 content to 0x%x : 0x%x",ich+1,CATIA_Reg6_def_, val),3);
        }
    } catch (std::exception &e ) {
        Log("[ECAL_Phase2_VICEpp::configureCATIA] Error configuring CATIA chips", 3);
    }
    Log("[ECAL_Phase2_VICEpp::configureCATIA] Configuring CATIA end...", 3);
}

int ECAL_Phase2_VICEpp::Read(std::vector<WORD> &v)
{
    _reset_fifo_cnt++;
    
    if (_debug) Log("[ECAL_Phase2_VICEpp::Read] entering...", 3);
    // the following are debugging lines:
    // ... free_mem = free memory on the FPGA buffer
    // ...  address = writing/reading addresses (16 bit each in a 32 bit word)
    ///free_mem = hw.getNode("CAP_FREE").read();
    ///address = hw.getNode("CAP_ADDRESS").read();
    ///hw.dispatch();
    ///if(debug>0)printf("address : 0x%8.8x, Free memory : %d",address.value(),free_mem.value());

    // write event header
    v.push_back(_header);

    // Read event samples from FPGA
    // loading into v

    for (auto & hw : _dv) {
        for(int itrans = 0; itrans < _n_transfer; ++itrans)
        {
            _block = hw.getNode ("CAP_DATA").readBlock(ECAL_Phase2_VICEpp_MAX_PAYLOAD / 4); // same value of MAX_PAYLOAD used in Marc software
            hw.dispatch();
            for(int is = 0; is < ECAL_Phase2_VICEpp_MAX_PAYLOAD / 4; ++is) {
                if (_debug > 1) _mem.push_back(_block[is]);
                v.push_back(_block[is]);
            }
        }
        _block = hw.getNode ("CAP_DATA").readBlock(_n_last);
        hw.dispatch();

        // if(_reset_fifo_cnt == 100)
        // {
        //     int command = ((_nsamples+1)<<16)+CAPTURE_STOP;
        //     hw.getNode("CAP_CTRL").write(command);
        //     hw.getNode("CAP_ADDRESS").write(0);
        //     command = ((_nsamples+1)<<16)+CAPTURE_START;
        //     hw.getNode("CAP_CTRL").write(command);
        //     hw.dispatch();

        //     _reset_fifo_cnt=0;
        // }
        
        for(int is = 0; is < _n_last; ++is) {
#ifdef DEBUGPAYLOAD //---use with caution
            std::cout << std::bitset<32>(_block[is]) << std::endl;
#endif
            v.push_back(_block[is]);
        }
    }
    if (_debug) Log("[ECAL_Phase2_VICEpp::Read] ...returning.", 3);
    return 0;
}


int ECAL_Phase2_VICEpp::ParseConfiguration(BoardConfig * bc)
{
    try {
        Log("[ECAL_Phase2_VICEpp::ParseConfiguration] entering...", 1);
        _manager_cfg             = bc->getElementContent("ManagerCfg");
        _devices                 = bc->getElementVector("Device");
        _nsamples                = Configurator::GetInt(bc->getElementContent("Nsamples").c_str());
        _trigger_self            = Configurator::GetInt(bc->getElementContent("TriggerSelf").c_str());
        _trigger_self_mode       = Configurator::GetInt(bc->getElementContent("TriggerSelfMode").c_str());
        _trigger_self_threshold  = Configurator::GetInt(bc->getElementContent("TriggerSelfThreshold").c_str());
        _trigger_self_mask       = Configurator::GetInt(bc->getElementContent("TriggerSelfMask").c_str());
        _trigger_soft            = Configurator::GetInt(bc->getElementContent("TriggerSoft").c_str());
        _trigger_soft_period     = Configurator::GetInt(bc->getElementContent("TriggerSoftPeriod").c_str());
        _hw_daq_delay            = Configurator::GetInt(bc->getElementContent("HwDAQDelay").c_str());
        _sw_daq_delay            = Configurator::GetInt(bc->getElementContent("SwDAQDelay").c_str());
        _debug                   = Configurator::GetInt(bc->getElementContent("DebugLevel").c_str());
        _calib_level             = Configurator::GetInt(bc->getElementContent("CalibLevel").c_str());
        _calib_width             = Configurator::GetInt(bc->getElementContent("CalibWidth").c_str());
        _calib_delay             = Configurator::GetInt(bc->getElementContent("CalibDelay").c_str());
        _calib_n                 = Configurator::GetInt(bc->getElementContent("CalibN").c_str());
        _calib_step              = Configurator::GetInt(bc->getElementContent("CalibStep").c_str());
        _negate_data             = Configurator::GetInt(bc->getElementContent("NegateData").c_str());
        _signed_data             = Configurator::GetInt(bc->getElementContent("SignedData").c_str());
        _input_span              = Configurator::GetInt(bc->getElementContent("InputSpan").c_str());

        TP_gain_ = 0;
        TP_gain_                 = Configurator::GetInt(bc->getElementContent("TP_GAIN").c_str());
        // Default content of Register 1 : SEU auto correction, no Temp output
        CATIA_Reg1_def_ = 0x02;
        CATIA_Reg1_def_          = stoi(bc->getElementContent("CATIA_REG1"), 0, 16);
        // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy
        CATIA_Reg4_def_ = 0x9000;
        CATIA_Reg4_def_          = stoi(bc->getElementContent("CATIA_REG4"), 0, 16);
        // Default content of Register 5 : DAC2 0
        CATIA_Reg5_def_ = 0x0000;
        CATIA_Reg5_def_          = stoi(bc->getElementContent("CATIA_REG5"), 0, 16);
        // Default content of Register 6 : Vreg ON, Rconv=2471, TIA_dummy ON, TP ON
        CATIA_Reg6_def_ = 0x0b;
        CATIA_Reg6_def_          = stoi(bc->getElementContent("CATIA_REG6"), 0, 16);

        DTU_force_G1_ = 0;
        DTU_force_G10_ = 0;
	// Force either gains. If both DTU_force_G1 and DTU_force_G10 are zero -> dynamic gain switch
	DTU_force_G1_ = Configurator::GetInt(bc->getElementContent("DTU_force_G1").c_str()); 
	DTU_force_G10_ = Configurator::GetInt(bc->getElementContent("DTU_force_G10").c_str());
	// Select gain switch window -> 0 -> 8 samples, 1 -> 16 samples
	DTU_window_length_ = Configurator::GetInt(bc->getElementContent("DTU_window_length").c_str());
        /*sscanf(bc->getElementContent("DTU_BULK1").c_str(), "%x", &DTU_bulk1_);
          sscanf(bc->getElementContent("DTU_BULK2").c_str(), "%x", &DTU_bulk2_);
          sscanf(bc->getElementContent("DTU_BULK3").c_str(), "%x", &DTU_bulk3_);
          sscanf(bc->getElementContent("DTU_BULK4").c_str(), "%x", &DTU_bulk4_);
          sscanf(bc->getElementContent("DTU_BULK5").c_str(), "%x", &DTU_bulk5_); */

        TP_level_ = 0;
        string content = bc->getElementContent("TP_LEVEL");
        if (configFieldIsValid("TP_LEVEL", content)) TP_level_ = Configurator::GetInt(content.c_str());

        //Log(Form("[ECAL_Phase2_VICEpp::ParseConfiguration] %u %u %u %u %u", DTU_bulk1_, DTU_bulk2_, DTU_bulk3_, DTU_bulk4_, DTU_bulk5_),1);
        
        // From <DEVICE> we extract the number of the board:
        // 1. we expect a single field Device in the configuration
        // 2. we expect it in the form fead.udp.XYZ
        string name_device = "fead.udp.";
        string single_device = bc->getElementContent("Device");
        string number_device = single_device.substr(name_device.length());
        Log(Form("[ECAL_Phase2_VICEpp::ParseConfiguration] Getting channel config for CHANNELCONFIG_%s!", number_device.c_str()),1);
        string node_name = "CHANNELCONFIG_" + number_device;


        xmlNode * ch_node = NULL;
        for (ch_node = bc->GetBoardNodePtr()->children; ch_node ; ch_node = ch_node->next) {
            if (ch_node->type == XML_ELEMENT_NODE && xmlStrEqual (ch_node->name, xmlCharStrdup (node_name.c_str()))  ) {
                xmlNode * ch_node1 = NULL;
                for (ch_node1 = ch_node->children; ch_node1 ; ch_node1 = ch_node1->next) {
                    if (ch_node1->type == XML_ELEMENT_NODE && xmlStrEqual (ch_node1->name, xmlCharStrdup ("CHANNEL"))  ) {
                        content = Configurable::getElementContent (*bc, "ID", ch_node1) ;
                        int ch = -1 ;
                        if (configFieldIsValid("ID", content)) ch = stoi(content);            
                        if (ch > 0 && ch < 6) {

                            // Defaults
                            pll_conf1_[ch-1]        = 4;
                            pll_conf2_[ch-1]        = 3;
                            pll_conf_[ch-1]         = 0x1c;

                            content = Configurable::getElementContent (*bc, "PLL_LOC", ch_node1);
                            if (configFieldIsValid("PLL_LOC", content)) {
                                sscanf(content.c_str(), "%x", &pll_loc_[ch-1]);
                                pll_conf_[ch-1]         = pll_loc_[ch-1]&0x01ff;
                                pll_conf1_[ch-1]        = pll_conf_[ch-1]&0x7;
                                pll_conf2_[ch-1]        = pll_conf_[ch-1]>>3;
                            }
                            
                            content = Configurable::getElementContent (*bc, "LOC_PED1", ch_node1);
                            if (configFieldIsValid("LOC_PED1", content)) pedestals_G1_[ch-1] = stoi(content, 0, 16);

                            content = Configurable::getElementContent (*bc, "LOC_PED10", ch_node1);
                            if (configFieldIsValid("LOC_PED10", content)) pedestals_G10_[ch-1] = stoi(content, 0, 16);

                            // Default content of Register 3 : 1.2V, LPF35, 400 Ohms, ped mid scale
                            CATIA_Reg3_def_[ch-1] = 0x1087;
                            CATIA_Reg3_def_[ch-1]=(pedestals_G1_[ch-1]<<8)|(pedestals_G10_[ch-1]<<3)|7;

                            // baseline_G1
                            content = Configurable::getElementContent (*bc, "BL_G1", ch_node1);
                            if (configFieldIsValid("BL_G1", content)) baseline_G1_[ch-1] = stoi(content);
                            
                            // baseline_G10
                            content = Configurable::getElementContent (*bc, "BL_G10", ch_node1);
                            if (configFieldIsValid("BL_G10", content)) baseline_G10_[ch-1] = stoi(content);

                            Log(Form("[ECAL_Phase2_VICEpp::ParseConfiguration] Channel %d bl_g1 %u, bl_g10 %u",ch, baseline_G1_[ch-1], baseline_G10_[ch-1] ),1);
                            Log(Form("[ECAL_Phase2_VICEpp::ParseConfiguration] Channel %d pll_loc %d, reg3 %d",ch, pll_loc_[ch-1], CATIA_Reg3_def_[ch-1] ),1);
                        
                        } else {
                            Log("[ECAL_Phase2_VICEpp::ParseConfiguration]::[ERROR] Found channel id outside 1-5 range",1);
                            return ERR_CONF_INVALID;
                        }   
                    }
                }
            }
        }
    } catch (const char* msg) {
        Log(msg,1);
        return ERR_CONF_INVALID;
    }
    return 0;
}

bool ECAL_Phase2_VICEpp::configFieldIsValid(std::string fieldName, std::string fieldValue) {
    if (fieldValue != "NULL") {
        return true;
    } else {
        Log("[ECAL_Phase2_VICEpp::ParseConfiguration]::[ERROR] Field " + fieldName + " not found in xml configuration",1);
        throw "Missing field in the configuration";
    }
    return false;
}

void ECAL_Phase2_VICEpp::Decode()
{}
// old dataformat 
// void ECAL_Phase2_VICEpp::Decode()
// {
//     Log("[ECAL_Phase2_VICEpp::Decode] entering...", 1);
//     // The first sample should have bit 70 at 1
//     if((_mem[0]>>31) != 1) Log(Form("Sample 0 not a header : %8.8x", _mem[0]), 1);
//     unsigned long int t1 =  _mem[0]     &0xFFFF;
//     unsigned long int t2 =  _mem[1]     &0xFFFF;
//     unsigned long int t3 = (_mem[1]>>16)&0xFFFF;
//     unsigned long int t4 =  _mem[2]     &0xFFFF;
//     unsigned long int t5 = (_mem[2]>>16)&0x00FF;
//     unsigned long int timestamp = (t5<<56) + (t4<<42) + (t3<<28) + (t2<<14) + t1;
//     Log(Form("timestamp : %8.8x %8.8x %8.8x",_mem[2],_mem[1],_mem[0]), 1);
//     Log(Form("timestamp : %ld %4.4lx %4.4lx %4.4lx %4.4lx %4.4lx", timestamp, t5, t4, t3, t2, t1), 1);
//     unsigned short int event[5]; // 5 channels per VFE
//     for(int is = 0; is < _nsamples; ++is)
//     {
//         int j = (is + 1) * 3;
//         event[0] =  _mem[j]       &0xFFFF;
//         if(_signed_data == 1 && ((event[0]>>13)&1) == 1) event[0]|=0xc000;
//         event[1] =  _mem[j+1]     &0xFFFF;
//         if(_signed_data == 1 && ((event[1]>>13)&1) == 1) event[1]|=0xc000;
//         event[2] = (_mem[j+1]>>16)&0xFFFF;
//         if(_signed_data == 1 && ((event[2]>>13)&1) == 1) event[2]|=0xc000;
//         event[3] =  _mem[j+2]     &0xFFFF;
//         if(_signed_data == 1 && ((event[3]>>13)&1) == 1) event[3]|=0xc000;
//         event[4] = (_mem[j+2]>>16)&0xFFFF;
//         if(_signed_data == 1 && ((event[4]>>13)&1) == 1) event[4]|=0xc000;
//         Log(Form("*** sample: %5d    blocks: %8.8x %8.8x %8.8x", is, _mem[j], _mem[j+1], _mem[j+2]), 1);
//         Log(Form("--> sample: %5d  channels: %8d %8d %8d %8d %8d", is, event[0], event[1], event[2], event[3], event[4]), 1);
//     }
//     Log("[ECAL_Phase2_VICEpp::Decode] ...returning.", 1);
// }


void ECAL_Phase2_VICEpp::Trigger()
{
    Log("[ECAL_Phase2_VICEpp::Trigger] sending trigger...", 3);
    for (auto & hw : _dv) {
        int command = 0;
        if (TP_level_ <= 0)
            command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*0+GENE_TRIGGER*1; // Pedestal trigger
        else 
            command = TP_MODE*0+LED_ON*1+GENE_100HZ*0+GENE_TP*1+GENE_TRIGGER*0; // TP trigger
                     
        hw.getNode("FW_VER").write(command);
        hw.dispatch();
    }
}


int ECAL_Phase2_VICEpp::Print()
{
    if (_debug) Log("[ECAL_Phase2_VICEpp::Print] entering...", 3);
    Log(Form("**** Parameters read from xml config file: %s", _manager_cfg.c_str()), 1);
    Log("  **  Device list:", 1);
    for (auto & d : _devices) {
        Log(Form("      .. %s", d.c_str()), 1);
    }
    Log(Form("  **  Number of samples      : %d", _nsamples              ), 1);
    Log(Form("  **  Self trigger           : %d", _trigger_self          ), 1);
    Log(Form("  **  Trigger soft           : %d", _trigger_soft          ), 1);
    Log(Form("  **  Trigger self threshold : %d", _trigger_self_threshold), 1);
    Log(Form("  **  Trigger self mask      : %d", _trigger_self_mask     ), 1);
    Log(Form("  **  Calib level            : %d", _calib_level           ), 1);
    Log(Form("  **  Calib width            : %d", _calib_width           ), 1);
    Log(Form("  **  Calib delay            : %d", _calib_delay           ), 1);
    Log(Form("  **  Calib N                : %d", _calib_n               ), 1);
    Log(Form("  **  Calib step             : %d", _calib_step            ), 1);
    Log(Form("  **  Negate data            : %d", _negate_data           ), 1);
    Log(Form("  **  Signed data            : %d", _signed_data           ), 1);
    Log(Form("  **  Input span             : %d", _input_span            ), 1);
    Log(Form("  **  HW DAQ delay           : %d", _hw_daq_delay          ), 1);
    Log(Form("  **  SW DAQ delay           : %d", _sw_daq_delay          ), 1);

    Log("**** Parameters read from registers of the single devices:", 1);
    for (auto & hw : _dv) {
        Log(Form("  ** Device: %s   uri: %s", hw.id().c_str(), hw.uri().c_str()), 1);
        // Read FW version to check :
        uhal::ValWord<uint32_t> reg = hw.getNode("FW_VER").read();
        // Cross-check the initialization
        // Read back delay values :
        uhal::ValWord<uint32_t> delays = hw.getNode("TRIG_DELAY").read();
        // Read back the read/write base address
        _address = hw.getNode("CAP_ADDRESS").read();
        _buffer_size = hw.getNode("CAP_FREE").read();
        uhal::ValWord<uint32_t> trig_reg = hw.getNode("VICE_CTRL").read();
        hw.dispatch();

        // Print init values
        Log(Form("     Firmware version      : %8.8x",   reg.value()), 1);
        Log(Form("     Delays                : %8.8x",   delays.value()), 1);
        Log(Form("     Initial R/W addresses : 0x%8.8x", _address.value()), 1);
        Log(Form("     Buffer Size           : 0x%8.8x", _buffer_size.value()), 1);
        Log(Form("     Trigger mode          : 0x%8.8x", trig_reg.value()), 1);
    }
    if (_debug) Log(Form("[ECAL_Phase2_VICEpp::Print] ...returning."), 3);
    return 0;
}


bool ECAL_Phase2_VICEpp::TriggerReceived()
{
    //--external trigger
    clock_gettime(CLOCK_MONOTONIC_RAW, &trig_time_);
    if (_trigger_soft == 0) {
        int timeout=0;
        while (1 && timeout<2000) {
            //check free memory
            for (auto & hw : _dv) {
                uhal::ValWord<uint32_t> free_mem = hw.getNode("CAP_FREE").read();
                hw.dispatch();
                // Log(Form("     Free mem           : 0x%8.8x", free_mem.value()), 1);
                if (free_mem.value() != _buffer_size.value()) //for the moment just signaling a trigger using an event present in memory
                    return 1;
            }
            usleep(30);
	    ++timeout;
        }
    } else if(_trigger_soft == 1) { //--self trigger
        usleep(_trigger_soft_period);
        Trigger();
        return 1;
    }
    return 0;
}


int ECAL_Phase2_VICEpp::SetBusyOn()
{
    return 0;
}


int ECAL_Phase2_VICEpp::SetBusyOff()
{
    return 0;
}


int ECAL_Phase2_VICEpp::TriggerAck()
{
    return 0;
}


int ECAL_Phase2_VICEpp::SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus)
{
    int status=0;
    if (triggerStatus == TRIG_ON) {
        status |= StopDAQ();
        status |= StartDAQ();
    } else if (triggerStatus == TRIG_OFF) {
        status |= StopDAQ();
    }
    return status;
}

#endif
