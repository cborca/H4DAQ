#include "interface/TimeBoard.hpp"
#include "interface/Utility.hpp"

TimeBoard::TimeBoard(): Board() {
    type_ = string("TIME") ;
}

int TimeBoard::Init(){
    // nothing to init
    return 0;
}

int TimeBoard::Clear(){
    //nothing to Clear
    return 0;
}

int TimeBoard::Print(){
    cout<<"Time Board: Current time is"<<(unsigned)time(NULL)<<endl;
    return 0;

}

int TimeBoard::Config(BoardConfig *bC){

    Board::Config(bC);	
    //to be configured
    Log("[TimeBoard]::[INFO]::Configured",1);
    return 0;
}

void TimeBoard::SetTimeRef()
{
    gettimeofday(&begin_of_spill_time_, NULL);
}

int TimeBoard::Read(vector<WORD> &v)
{
    v.clear();
    //WORD x=(WORD) ((unsigned)time(NULL) );
    // timeval tv;
    // gettimeofday(&tv,NULL);
    struct timeval t;
    timeradd(&trig_time_,&begin_of_spill_time_,&t);
    time_t ref=0; //not using a reference to an arbitrary date
    unsigned long x	= Utility::timestamp(&t, &ref);
    unsigned long x_prime = x>>32;
    WORD x_msb = (( x_prime ) & 0xFFFFFFFF ); 
    WORD x_lsb = x & (0xFFFFFFFF);
    //printf("[TimeBoard]::[Read] %lu xprime=%lu %X %X\n",x,x_prime,x_lsb,x_msb); //DEBUG
    v.push_back(x_lsb);
    v.push_back(x_msb);
    return 0;
}
