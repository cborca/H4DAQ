#ifndef NO_BCP
#include <iostream>
#include <interface/Logger.hpp>
#include <interface/BCP.hpp>
#include <interface/Board.hpp>
#include <memory>
#include <map>
#include <string> 
#include <functional>
#include <chrono> 


using std::shared_ptr; 


int main() {


  Logger l;
  l.SetFileName("bcp_log.log"); 
  l.SetLogLevel(3); 
  l.Init();
  
  std::string roguePath = "/home/cmsdaq/miniconda3/envs/H4DAQ/lib/python3.7/site-packages"; 
  std::string pyPath = "/home/cmsdaq/bcp_controller_Ntower"; 

  shared_ptr<BCP> bcpobj(new BCP(true, "192.168.41.192", 10000, true,
				     pyPath, 
				 roguePath) ); 
  //bcpobj->enableSWTrig(); 
  bcpobj->LogInit(&l); 
  bcpobj->Init();
  bcpobj->setupReadout(); 
  bcpobj->SetBusyOn(); 
  for (int i = 0; i < 10; i++) {
    std::this_thread::sleep_for(1s); 
    bcpobj->readoutStatus(); 
    std::cout << i << " seconds" << std::endl; 
  }
  bcpobj->SetBusyOff(); 

  std::vector<WORD> v; 
  bcpobj->Readout(); 
  int c = 0; 

  auto end = std::chrono::system_clock::now() + std::chrono::seconds(10); 
  
  auto past = std::chrono::system_clock::now(); 
  bool busy = bcpobj->readoutBusy(); 
  while(busy) { 
    auto curr = std::chrono::system_clock::now(); 
    std::chrono::duration<float> diff = curr - past; 
    if (diff.count() > 0.5) {
      busy =  bcpobj->readoutBusy(); 
      std::cout << "Still busy...." << busy << std::endl; 
      past = curr; 
    }

    if (curr > end) {
      std::cout << "Timed out waiting for readout" << std::endl; 
      break;
    }

  }

  bcpobj->Read(v); 
  std::cout << v.size() << std::endl; 
  std::cout << "End busy" << std::endl; 

  bcpobj = nullptr; 
  
  return 0; 

}

#else

int main() {}

#endif
